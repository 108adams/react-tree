export interface INodeDescription {
    type: string;
    props: {
        store: any,
        elements? :INodeDescription[]
        label?: string
        key?:number
    }
}
