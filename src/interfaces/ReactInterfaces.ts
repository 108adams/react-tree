export interface MyProps {
    elements?: any[]; // FIXME for index.ts
    description?: {
        elements?: any[];
        label?: string;
        initialValue?: any;
        key?: any;
        value?: any;
    }
}

export interface MyState {
    value?: any;
    children?: any[];
}