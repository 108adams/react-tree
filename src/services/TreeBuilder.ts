import {factory} from '../components/factory';

import {INodeDescription} from '../interfaces/INodeDescription';

export function createChildrenFromDescription(childrenDescription: INodeDescription[]): any[] {
    if (!childrenDescription || childrenDescription.length === 0) {
        return null;
    }

    return childrenDescription.map(
        (childDescription, index) => createChild(childDescription, index)
    );
}

function createChild(childDescription: INodeDescription, key: number): any {
    childDescription.props.key = key;
    return factory(childDescription.type, childDescription.props);
}
