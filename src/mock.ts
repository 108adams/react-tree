export const mock = {
    type: 'root',
    props: {
        elements: [
            {
                type: 'form',
                props: {
                    elements: [
                        {
                            type: 'input',
                            props: {
                                label: 'input label'
                            }
                        },
                        {
                            type: 'checkbox',
                            props: {
                                label: 'checkbox label'
                            }
                        },
                        {
                            type: 'button',
                            props: {
                                label: 'button label'
                            }
                        },
                        {
                            type: 'static',
                            props: {
                                label: 'static text'
                            }
                        }
                    ]
                }
            }

        ]
    }
};
