import * as React from 'react';
import {connect} from 'react-redux';

interface TStateProps {
    description?: any;
    buttonValue?: boolean;
}

const mapStateToProps = (state: any): TStateProps => ({
    buttonValue: state.button.buttonValue
});

@connect<TStateProps, any, any>(
    mapStateToProps
)
export class Static extends React.Component<TStateProps, any> {

    constructor(props) {
        super(props);
    }

    render() {
        return <span key={this.props.description.key}>Button is: {this.props.buttonValue ? "on": "off"}</span>;
    }
}