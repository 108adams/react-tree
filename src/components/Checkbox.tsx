import * as React from 'react';
import {connect} from "react-redux";

interface MyState {
}

interface IDispatchProps {
}

interface MyProps {
    description: any;
}

@connect()
export class Checkbox extends React.Component<MyProps & IDispatchProps, MyState> {

    constructor(props) {
        super(props);
    }

    render() {
        return <input key={this.props.description.key} type="checkbox" />;
    }
}