import * as React from 'react';
import {connect, Dispatch} from 'react-redux';
import {toggleButton} from "../redux/actions";

// props passed from parent, extended by redux
interface TStateProps {
    description?: any;
    buttonValue?: boolean;
}

interface TDispatchProps {
}

// dynamically created props by redux#mapDispatchToProps
interface TOwnProps {
    toggleState: any;
}

const mapStateToProps = (state: any): TStateProps => ({
    buttonValue: state.button.buttonValue
});

const mapDispatchToProps = (dispatch: Dispatch<any>): TOwnProps => ({
    toggleState: (newValue: boolean): void => dispatch(toggleButton(newValue))
});

@connect<TStateProps, TDispatchProps, TOwnProps>(
    mapStateToProps,
    mapDispatchToProps
)
export class Button extends React.Component<TStateProps & TDispatchProps, any> {

    constructor(props) {
        super(props);
    }

    render() {
        return <button
            key={this.props.description.key}
            onClick={handleOnClick.bind(this)}
        >
            {this.props.description.label}
        </button>
    }
}

function handleOnClick(e) {
    this.props.toggleState(!this.props.buttonValue);
    e.preventDefault();
}
