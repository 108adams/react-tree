import * as React from 'react';

import {Form} from './Form';
import {Input} from './Input';
import {Checkbox} from './Checkbox';
import {Button} from './Button';
import {Static} from "./Static";

export function factory(type: string, childDescription: any) :any {

    switch (type) {
        case 'form':
            return <Form key={childDescription.key} description={childDescription} />;
            // return React.createElement(Form, {key: childDescription.key, description: childDescription});
        case 'input':
            return <Input key={childDescription.key} description={childDescription} />;
        case 'checkbox':
            return <Checkbox key={childDescription.key} description={childDescription} />;
        case 'button':
            return <Button key={childDescription.key} description={childDescription} />;
        case 'static':
            return <Static key={childDescription.key} description={childDescription} />;
        default:
            throw new Error('no such element implemented: ' + type);
    }
}
