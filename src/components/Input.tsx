import * as React from 'react';
import {connect} from 'react-redux';

interface TStateProps {
    description: any
}

interface TDispatchProps {
}

interface TOwnProps {
}

@connect()
export class Input extends React.Component<TStateProps & TDispatchProps, any> {

    constructor(props) {
        super(props);
    }

    render() {
        return <input
            key={this.props.description.key}
            type="text"
            value={this.props.description.initialValue}
        />;
    }
}