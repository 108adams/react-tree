import * as React from 'react';
import {connect} from "react-redux";
import {createChildrenFromDescription} from '../services/TreeBuilder';

interface MyState {
}

interface IDispatchProps {
}

interface MyProps {
    description: any;
}

@connect()
export class Form extends React.Component<MyProps & IDispatchProps, MyState> {

    constructor(props) {
        super(props);
    }

    render() {
        let children = createChildrenFromDescription(this.props.description.elements);

        return <form key={this.props.description.key} className="frm">
            {children}
        </form>;
    }
}