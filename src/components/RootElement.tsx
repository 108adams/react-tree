import * as React from 'react';
import {connect} from "react-redux";
import {createChildrenFromDescription} from '../services/TreeBuilder';

interface MyState {
}

interface IDispatchProps {
}

interface MyProps {
    elements: any;
}

@connect()
export class RootElement extends React.Component<MyProps & IDispatchProps, MyState> {

    constructor(props) {
        super(props);
    }

    render() {
        let children = createChildrenFromDescription(this.props.elements);

        return <div>
            {children}
            </div>;
    }
}