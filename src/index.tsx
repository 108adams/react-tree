import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {app} from './redux/reducers';

import {RootElement} from './components/RootElement';

import {mock} from './mock';

let store = createStore(app,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <Provider store={store}>
        <RootElement elements={mock.props.elements} />
    </Provider>,
    document.getElementById('app')
);
