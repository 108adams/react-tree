export function toggleButton (value: boolean): any {
    return {
        type: 'TOGGLE_BUTTON',
        value
    }
}
