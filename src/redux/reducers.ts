import {combineReducers} from 'redux';
import * as _ from 'lodash';

const initialState = {
    buttonValue: false
};

function button(state = initialState, action) {
    switch (action.type) {
        case 'TOGGLE_BUTTON':
            return _.assign({}, state, {buttonValue: action.value});
        default:
            return state;
    }
}

export const app = combineReducers({
    button
});
