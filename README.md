# README #

Basic example of a React-based tree structure generator using JSON description.

Uses TypeScript 2+ with support for JSX/TSX, React/Redux combo, built with browserify.

I highly advice reading _packages.json_ and _tsconfig.json_ to see an example of minimal working and extremely efficient configuration.

# Building #

```
npm run build
```

It will emit a bundle.js file containing all the stuff.

To run devserver _budo_, a watching wrapper for browserify with http server included: 

```
npm start
```

Open [http://192.168.56.1:9966/](http://192.168.56.1:9966/)

# DevTools #

Please install React and Redux devtools in your browser for maximized pleasure and joy.

# How to read the code #

Start with `index.tsx`, then go the React way: leave `app` and redux stupp aside and see `components/*.tsx`. Read Redux stuff at the end. Ask. 